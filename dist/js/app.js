"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// -------- START : Imports
// Instruments
var instruments_json_1 = __importDefault(require("../json/instruments.json")); // This import style requires "esModuleInterop", see "side notes"
console.log(instruments_json_1.default[0]);
// -------- END : Imports --------
window.addEventListener('load', function () {
    // -------- START : Instrument section --------
    var instrumentsContainer = document.querySelector('#instruments');
    if (instrumentsContainer) {
        console.log('Instruments ', instruments_json_1.default);
    }
    // -------- END : Instrument section --------
});
