"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// -------- START : Imports
// Instruments
const instruments_json_1 = __importDefault(require("../json/instruments.json"));
// -------- END : Imports --------
window.addEventListener('load', () => {
    // -------- START : Instrument section --------
    const instrumentsContainer = document.querySelector('#instruments');
    if (instrumentsContainer) {
        console.log('Instruments ', instruments_json_1.default);
    }
    // -------- END : Instrument section --------
});
