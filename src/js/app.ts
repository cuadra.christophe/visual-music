// -------- START : Imports
// Instruments
import colorsJson from '../json/instruments.json'; // This import style requires "esModuleInterop", see "side notes"
console.log(colorsJson[0]);
// -------- END : Imports --------

window.addEventListener('load', () => {
  // -------- START : Instrument section --------
  const instrumentsContainer = document.querySelector('#instruments');
  if (instrumentsContainer) {
    console.log('Instruments ', colorsJson);
  }
  // -------- END : Instrument section --------
})